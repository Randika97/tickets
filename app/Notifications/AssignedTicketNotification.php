<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AssignedTicketNotification extends Notification
{
    use Queueable;

    
    public function __construct($ticket)
    {
        $this->ticket = $ticket;
    }

    
    public function via($notifiable)
    {
        return ['mail'];
    }

    
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('You have been assigned a new ticket')
                    ->greeting('Hello,')
                    ->line('You have been assigned a new ticket: '.$this->ticket->title)
                    ->action('View ticket', route('admin.tickets.show', $this->ticket->id))
                    ->line('Thank you')
                    ->line(config('app.name') . ' Team')
                    ->salutation(' ');
    }
}