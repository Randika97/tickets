<?php

namespace App\Http\Controllers;

use App\tick;
use Illuminate\Http\Request;

class TickController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tick.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'         => 'required',
            'content'       => 'required',
            'author_name'   => 'required',
            'author_email'  => 'required|email',
        ]);
        
        $request->request->add([
            'category_id'   => 1,
            'status_id'     => 1,
            'priority_id'   => 1
        ]);
        
        $ticket = Ticket::create($request->all());
        
       
        
        return redirect()->back()->withStatus('Your ticket has been submitted<a href="'.route('tickets.show', $ticket->id).'">Click here</a>');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\tick  $tick
     * @return \Illuminate\Http\Response
     */
    public function show(tick $tick)
    {
        $tick->load('comments');
        
        return view('tickets.show', compact('ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tick  $tick
     * @return \Illuminate\Http\Response
     */
    public function edit(tick $tick)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tick  $tick
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tick $tick)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tick  $tick
     * @return \Illuminate\Http\Response
     */
    public function destroy(tick $tick)
    {
        //
    }
    
    public function storeComment(Request $request, tick $tick)
    {
        $request->validate([
            'comment_text' => 'required'
        ]);
        
        $comment = $ticket->comments()->create([
            'author_name'   => $ticket->author_name,
            'author_email'  => $ticket->author_email,
            'comment_text'  => $request->comment_text
        ]);
        
        $tick->sendCommentNotification($comment);
        
        return redirect()->back()->withStatus('comment added');
    }
}
